from datetime import date

from django.contrib import admin
from django import forms
from django.contrib.auth.models import Group
from rest_framework.authtoken.models import Token

from download_app import models


class UserCreationForm(forms.ModelForm):
    request = None

    class Meta:
        model = models.User
        fields = '__all__'
        widgets = {'password': forms.PasswordInput(render_value=True)}

    def save(self, commit=True):
        user = super().save(commit=False)
        if not self.instance.pk:
            user.creator = self.request.user
        if commit:
            user.save()
        return user


class UserAdmin(admin.ModelAdmin):
    form = UserCreationForm
    fields = ('creator', 'is_active', 'plan', 'username', 'password', 'full_name', 'max_mac_allowance', 'login_history')
    readonly_fields = ('login_history', 'creator')

    class Meta:
        model = models.User

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        obj.is_admin = False
        if 'password' in form.changed_data:
            obj.set_password(form.cleaned_data['password'])
        obj.save()

    def get_queryset(self, request):
        if request.user.is_superuser:
            return models.User.objects.filter(is_superuser=False, is_admin=False)
        else:
            return models.User.objects.filter(creator=request.user, is_admin=False)

    def get_form(self, request, obj=None, **kwargs):
        self.form.request = request
        return super().get_form(request, obj, **kwargs)


class SubAdminCreationForm(forms.ModelForm):
    request = None

    class Meta:
        model = models.SubAdmin
        fields = '__all__'
        widgets = {'password': forms.PasswordInput(render_value=True)}

    def save(self, commit=True):
        user = super().save(commit=False)
        if not self.instance.pk or 'password' in self.changed_data:
            user.is_admin = True
            user.set_password(self.cleaned_data["password"])
        if not self.instance.pk:
            user.creator = self.request.user
        if commit:
            user.save()
        return user


class SubAdmin(admin.ModelAdmin):
    form = SubAdminCreationForm
    fields = ('username', 'password', 'full_name')

    class Meta:
        model = models.SubAdmin

    def get_queryset(self, request):
        if request.user.is_superuser:
            return models.SubAdmin.objects.filter(is_superuser=False, is_admin=True)
        else:
            return models.SubAdmin.objects.filter(creator=request.user, is_admin=True)

    def get_form(self, request, obj=None, **kwargs):
        self.form.request = request
        return super().get_form(request, obj, **kwargs)

    def has_module_permission(self, request):
        return request.user.is_superuser


class APKCategoryAdmin(admin.ModelAdmin):
    class Meta:
        model = models.APKCategory

    def has_module_permission(self, request):
        return request.user.is_superuser


class APKAdmin(admin.ModelAdmin):

    class Meta:
        model = models.APKApp

    def has_module_permission(self, request):
        return request.user.is_superuser


class QuestionAnswerInline(admin.StackedInline):
    model = models.QuestionAnswer


class FAQAdmin(admin.ModelAdmin):
    inlines = [QuestionAnswerInline]

    class Meta:
        model = models.FAQs

    def has_module_permission(self, request):
        return request.user.is_superuser

    def has_add_permission(self, request):
        return models.FAQs.objects.all().count() < 1


admin.site.unregister(Group)
admin.site.unregister(Token)
admin.site.register(models.User, UserAdmin)
admin.site.register(models.SubAdmin, SubAdmin)
admin.site.register(models.APKCategory, APKCategoryAdmin)
admin.site.register(models.APKApp, APKAdmin)
admin.site.register(models.FAQs, FAQAdmin)
