from django.db import models

from simple_login import models as dsl_models

SUBSCRIPTION_PLAN_UNLIMITED = -1
SUBSCRIPTION_PLAN = (
    (SUBSCRIPTION_PLAN_UNLIMITED, 'Unlimited'),
    (7, '7 Days'),
    (14, '14 Days'),
    (30, '30 Days'),
    (180, '6 Months'),
    (360, '1 Year'),
    (1080, '3 Years'),
)


class User(dsl_models.BaseUser):
    account_activation_sms_otp = None
    password_reset_sms_otp = None

    # email = models.EmailField(max_length=255, blank=False, unique=True)
    username = models.CharField(max_length=255, blank=False, unique=True)
    full_name = models.CharField(max_length=255, blank=False)

    creator = models.ForeignKey('User', on_delete=models.SET_NULL, null=True)

    plan = models.IntegerField(blank=False, choices=SUBSCRIPTION_PLAN, default=SUBSCRIPTION_PLAN_UNLIMITED)
    subscription_date = models.DateField(auto_now=True)
    max_mac_allowance = models.IntegerField(blank=False, default=0)
    login_history = models.TextField(blank=True)

    def __str__(self):
        return self.username


class SubAdmin(User):
    class Meta:
        proxy = True

    def __str__(self):
        return self.username if self.username else self.email


class APKCategory(models.Model):
    name = models.CharField(max_length=255, blank=False)

    def __str__(self):
        return self.name


class APKApp(models.Model):
    label = models.CharField(max_length=255, blank=False)
    screen_grab = models.ImageField(blank=False)
    apk = models.TextField(blank=False)
    extension = models.CharField(max_length=64, blank=False, null=True)
    category = models.ForeignKey(APKCategory, on_delete=models.CASCADE)

    def __str__(self):
        return self.label


class FAQs(models.Model):
    heading = models.CharField(max_length=255, blank=False, default='Frequently asked questions.')

    def __str__(self):
        return self.heading

    class Meta:
        verbose_name = 'FAQs'
        verbose_name_plural = 'FAQs'


class QuestionAnswer(models.Model):
    root = models.ForeignKey(FAQs, on_delete=models.CASCADE, related_name='qas')
    question = models.CharField(max_length=255, blank=False)
    answer = models.CharField(max_length=1000, blank=False)


class MACAddress(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    mac = models.CharField(max_length=255, blank=False)
