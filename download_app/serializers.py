from rest_framework import serializers

from download_app import models


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = models.User
        fields = ('id', 'email', 'password', 'full_name')


class APKCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.APKCategory
        fields = '__all__'


class APKSerializer(serializers.ModelSerializer):
    category = APKCategorySerializer()

    class Meta:
        model = models.APKApp
        fields = '__all__'


class QuestionAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.QuestionAnswer
        fields = ('question', 'answer', )
