from datetime import date, timedelta

from download_app import models


def check_subscriptions():
    for user in models.User.objects.filter(is_superuser=False, is_admin=False):
        if not user.is_active or user.plan == models.SUBSCRIPTION_PLAN_UNLIMITED:
            continue
        if (user.subscription_date + timedelta(days=user.plan)) <= date.today():
            user.is_active = False
            user.save()
