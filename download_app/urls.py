from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from download_app import views


urlpatterns = [
    url(r'^api/login$', views.LoginAPIView.as_view()),
    url(r'^api/apks$', views.ListAPKAPIView.as_view()),
    url(r'^api/apk-categories$', views.ListAPKCategoriesAPIView.as_view()),
    url(r'^api/faqs$', views.QuestionAnswerListAPIView.as_view()),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
