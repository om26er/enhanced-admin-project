from datetime import datetime

from rest_framework import generics, status, permissions, response
from simple_login import views

from download_app import models, serializers


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        return x_forwarded_for.split(',')[0]
    return request.META.get('REMOTE_ADDR')


class LoginAPIView(views.LoginAPIView):
    user_model = models.User
    serializer_class = serializers.UserSerializer

    def post(self, *args, **kwargs):
        if 'mac' not in self.request.data:
            return response.Response(data={'mac': 'Field missing'}, status=status.HTTP_400_BAD_REQUEST)
        res = super().post(*args, **kwargs)
        if res.status_code == status.HTTP_200_OK:
            user = models.User.objects.get(id=res.data.get('id'))
            macs = models.MACAddress.objects.filter(user=user)
            is_mac_already_added = models.MACAddress.objects.filter(
                user=user, mac=self.request.data.get('mac')).exists()

            def deactivate():
                user.is_active = False
                user.save()
                return response.Response(data={'detail': 'User deactivated by admin.'},
                                         status=status.HTTP_403_FORBIDDEN)

            if macs.count() == user.max_mac_allowance:
                if not is_mac_already_added:
                    return deactivate()
            elif macs.count() > user.max_mac_allowance:
                return deactivate()

            models.MACAddress.objects.get_or_create(user=user, mac=self.request.data.get('mac'))
            line = "IP: {}, TIME: {}\n".format(get_client_ip(self.request),
                                               datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            user.login_history += line
            user.save()
        return res


class ListAPKAPIView(generics.ListAPIView):
    queryset = models.APKApp.objects.all()
    serializer_class = serializers.APKSerializer
    permission_classes = (permissions.IsAuthenticated,)


class ListAPKCategoriesAPIView(generics.ListAPIView):
    queryset = models.APKCategory.objects.all()
    serializer_class = serializers.APKCategorySerializer
    permission_classes = (permissions.IsAuthenticated, )


class QuestionAnswerListAPIView(generics.ListAPIView):
    serializer_class = serializers.QuestionAnswerSerializer
    queryset = models.QuestionAnswer.objects.all()
